export default class Ile extends Phaser.Physics.Arcade.Sprite {

 constructor(scene, x, y, numeroImage) {
    let images = ['ileA', 'ileB', 'ileC', 'ileD'];
    super(scene, x, y, images[numeroImage]); //On appelle le constructeur d'image

    scene.add.existing(this);
    scene.physics.add.existing(this); //ajouter à la physique sinon ça apparait pas
  }

  changerVitesse(vitesse) {
   this.setVelocityY(vitesse);
  }

}
