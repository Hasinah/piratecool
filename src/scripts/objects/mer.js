export default class Mer extends Phaser.Physics.Matter.Sprite {
   scene

   constructor(scene) {
      super(scene.matter.world, 0, -720, 'merAnim', 0, { frictionAir: 0, isSensor: true, vertices: [{ x: 0, y: 0 }, { x: 0, y: 0.0001 }, { x: 0.0001, y: 0.0001 }] }); //On appelle le constructeur d'image
      this.scene = scene;

      this.setCollidesWith(0);

      this.setOrigin(0, 0);    //On controle l'image depuis le coin car c'est plus simple ainsi pour la faire défiler

      scene.add.existing(this);

      this.play('merEau');

   }


   //A chaque rafraichissement de l'écran
   update() {

      const margeTexture = 50;

      let deltaY = this.scene.cameras.main.worldView.y - this.y;
      if (deltaY <= 0+margeTexture) {
         this.y -= 720;
      }
      if (deltaY > 720+margeTexture) {
         this.y += 720;
      }
   }


}
