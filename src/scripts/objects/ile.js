import Dynamique from '../objects/dynamique.js'

export default class Ile extends Dynamique {
numeroImage


   constructor(iles, scene, x, y, numeroImage) {
      let ileA = '130 286 35 251 0 150 64 84 116 60 112 21 133 0 162 11 184 54 237 51 264 66 281 113 284 185 257 251 239 272 181 286';
      let ileB = '107 16 154 63 235 53 243 27 262 24 270 5 321 27 347 9 378 34 394 108 369 149 376 230 323 350 225 363 147 395 61 365 4 258 2 145 21 101 2 37 19 0';
      let ileC = '344 150 362 170 339 184 377 225 359 270 351 366 285 380 259 340 251 298 233 277 213 350 111 344 96 378 64 382 13 356 0 300 47 236 83 241 129 225 157 203 88 193 52 171 26 86 55 35 99 9 174 10 231 15 282 42 350 72 359 86 351 108 344 120';
      let ileD = '118 351 66 298 38 245 11 191 11 84 71 41 178 17 269 22 341 47 386 139 397 271 327 346 229 368';
      let ileVertexArray = [ileA, ileB, ileC, ileD];
      let images = ['ileA', 'ileB', 'ileC', 'ileD'];

      super(iles, scene, x, y, images[numeroImage], 0, { shape : { type: 'fromVerts', verts: ileVertexArray[numeroImage], flagInternal: true }, frictionAir: 0.0, label: 'ile', isStatic: true }); //On appelle le constructeur d'image

      this.numeroImage = numeroImage;
   }

   update() {
   }


  lancerAnimation(){
      let animations = ['ileAEau', 'ileBEau', 'ileCEau', 'ileDEau'];
      this.play(animations[this.numeroImage]);
    }
}
