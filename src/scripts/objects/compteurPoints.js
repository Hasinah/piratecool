export default class CompteurPoints extends Phaser.GameObjects.Text {
  points
  scene

  constructor(scene, nbrPoints) {
    super(scene, 20, 5, '', { color: '#ffffff',
     fontSize: '28px',
     stroke: '#000000',
        strokeThickness: 3,
        fontStyle: 'bold'
        })
    scene.add.existing(this)
    this.setOrigin(0)

    this.points = nbrPoints;
    this.scene = scene;
    this.setDepth(50);
  }

  update() {
    this.setText(`Pièces: ${this.points}\nNiveau : ${this.scene.niveau + 1}`)
    this.y = this.scene.cameras.main.worldView.y + 5;
  }

  ajouterPoint() {
    this.points += 1;
  }
}
