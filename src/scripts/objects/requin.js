import Dynamique from '../objects/dynamique.js'

export default class Requin extends Dynamique {
//direction
mirroir

   constructor(requins, scene, x, y) {
      let requin = '-37 26.5 -71 8.5 -12 -36.5 -2 -12.5 30 -1.5 68 -28.5 67 15.5 41 8.5 3 17.5 6 33.5 -14 26.5';
      let requinMirroir = ' 11 26.5 -9 33.5 -6 17.5 -44 8.5 -70 15.5 -71 -28.5 -33 -1.5 -1 -12.5 9 -36.5 68 8.5 34 26.5';
      let forme = requin;

      let mirroir = false;
      if(x < scene.cameras.main.width/2) {
         mirroir = true;
         forme = requinMirroir;
      }


      super(requins, scene, x, y, 'requin', 0, { shape : { type: 'fromVerts', verts: forme, flagInternal: true }, frictionAir: 0.1, label: 'requin' }); //On appelle le constructeur d'image
      
      this.setMass(5000);

      if(mirroir){this.setFlipX(true);}
      this.setOrigin(0.5,0.5);

      this.mirroir = mirroir;
      this.vieRestante = -1;
   }

   update() {

      //rotation
     let currentPoint = new Phaser.Geom.Point(this.x,this.y);
     let pointToMoveTo = new Phaser.Geom.Point(this.scene.bateau.x, this.scene.bateau.y);
     let extraTotation = 0;
     if(!this.mirroir) {extraTotation = Phaser.Math.TAU*2};
     this.rotation = Phaser.Math.Angle.BetweenPoints(currentPoint, pointToMoveTo) + extraTotation;


     //mouvement
     let vecteurMouvement = new Phaser.Math.Vector2({x: -1, y: 0});
     if(this.mirroir) {
        vecteurMouvement = new Phaser.Math.Vector2({x: 1, y: 0});
     }


     let vecteur = vecteurMouvement.rotate(this.rotation).scale(1.5);
     this.scene.matter.body.applyForce(this.body, this.body.position, vecteur);
   }


}
