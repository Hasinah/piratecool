import Dynamique from '../objects/dynamique.js'


export default class GroupeDynamique {
  vitesse
  arrayDynamique
  scene



  constructor(scene) {
    this.scene = scene;
    this.arrayDynamique = [];
  }


  update() {
    this.arrayDynamique.forEach(child => child.update());    

    const distanceSuppressionObjetHorsEcran = 1200;
    this.retirerVieuxDynamiques(distanceSuppressionObjetHorsEcran);
  }


  getLabel() {
    alert("GroupeDynamique - getLabel: Fonction virtuelle, veuillez surcharger");
  }

  newDynamique(positionX, positionY) {
    alert("GroupeDynamique - newDynamique: Fonction virtuelle, veuillez surcharger");
    return new Dynamique(this, this.scene, positionX, positionY);
  }

  ajouterDynamique(positionX, positionY, apparaitPetit) {
    let dynamique = this.newDynamique(positionX, positionY, apparaitPetit); //remplacé par la classe qui hérite

     
    if (this.scene.matter.overlap(dynamique) && !apparaitPetit) {
      dynamique.destroy();
    }
    else {
      let copieThis = this;

      this.scene.matter.world.on("collisionstart", (event, bodyA, bodyB) => {
        if (bodyA.label === this.getLabel()) {
          this.collisionObjet(bodyA, bodyB); //remplacé par la classe qui hérite
        }
        if (bodyB.label === this.getLabel()) {
          this.collisionObjet(bodyB, bodyA);                    
        }
      });    
            
      this.arrayDynamique.push(dynamique);
    }

    return dynamique;
  }


  collisionObjet(bodyDynamique, bodyAutre) {
    alert("GroupeDynamique - collisionObjet: Fonction virtuelle, veuillez surcharger");
  }



  retirerVieuxDynamiques(limite) {
    let dynamiques = this;

    this.arrayDynamique.forEach(function(dynamique) {
      if (dynamique.y > dynamiques.scene.cameras.main.y+limite) {
        /*
          Phaser + Matter.js semble ne pas aimer qu'on supprime les objets dynamiquement,
          cause lag et boucles quasi-infinies durant l'update du moteur physique.
          Solution : Les perfs sont suffisantes sans retirer les objets hors de vue. On retire l'optimisation.
          Note : Pas trouvé d'info sur le sujet. Matter est très mal documenté et Phaser utilise une version modifiée.

          dynamiques.retirerDynamique(dynamique); 
        */
      }
    });
  }

  retirerDynamique(dynamique) {
    let id = this.arrayDynamique.findIndex(child => child === dynamique); // on retire l'dynamique
    this.arrayDynamique[id].destroy();
    this.arrayDynamique.splice(id, 1);;
  }


  getDynamiqueParBody(body) {
    let id = this.arrayDynamique.findIndex(child => child === body.gameObject); // on retire l'dynamique
    if (id === -1) {
      console.log("Can't find ", body, this.getLabel());
      this.arrayDynamique.forEach(child => console.log(child.id)); // on retire l'dynamique
      this.arrayDynamique.forEach(child => console.log(child.body)); // on retire l'dynamique
      this.arrayDynamique.forEach(child => child.test()); // on retire l'dynamique
    }
    return this.arrayDynamique[id];
  }

}
