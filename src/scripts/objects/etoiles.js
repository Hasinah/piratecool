import Etoile from '../objects/etoile.js'
import GroupeDynamique from '../objects/groupeDynamique.js'

export default class Etoiles extends GroupeDynamique {

    //Notre bateau joueur
    constructor(scene, arrayEtoiles) {
        super(scene, arrayEtoiles);
    }

    getLabel() {
        return 'etoile';
    }

    newDynamique(positionX, positionY, apparaitPetit) {
        return new Etoile(this, this.scene, positionX, positionY, apparaitPetit);
    }

    collisionObjet(bodyEtoile, bodyAutre) {
        if (bodyAutre.label === 'bateau') {
            if (this.getDynamiqueParBody(bodyEtoile).estPasTouche()) {

                this.getDynamiqueParBody(bodyEtoile).lancerDestruction();
                this.getDynamiqueParBody(bodyEtoile).setCollidesWith(0);

                this.scene.compteurPoints.ajouterPoint();  
                this.scene.sonPiece.play();          
            }
        }
        else {
        }
    }

}
