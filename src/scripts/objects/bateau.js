export default class Bateau extends Phaser.Physics.Matter.Sprite { // Sprite -> peut être animé
    //nos variables contenu dans cet objet
    vitesse
    vitesseLaterale
    scene
    acceleration

    desactiverControles
    estLance

    imagePirate
    lestouches


    //Notre bateau joueur
    constructor(scene, x, y, vitesse, acceleration) {

        var bateau = '-57 182 -83 111 -90 20 -88 -61 -69 -112 -2 -197 61 -117 83 -58 86 29 80 110 52 182';
        super(scene.matter.world, x, y, 'bateau', 0, { shape : { type: 'fromVerts', verts: bateau, flagInternal: true }, frictionAir: 0, label: 'bateau' });
        this.scene = scene;

        this.setMass(1000);
        this.setFixedRotation();
        this.setOrigin(0.5,0.5); //on conduit le bateau depuis le centre

        scene.add.existing(this); //ajoute a la scene sinon ça apparait pas

        //this.changerVitesse(this.vitesse);
        this.lestouches = scene.input.keyboard.createCursorKeys();
        this.vitesse = vitesse;
        this.acceleration = acceleration;
        this.desactiverControles = false;
        this.estLance = false;

        this.imagePirate = this.scene.add.sprite(x, y-150, 'pirate');


    }


    update() {

        this.limiterPosition();

        this.bougerPirate();

        this.faireAnimations();


        if (!this.desactiverControles) {


            //this.changerVitesse(this.vitesse);
            //on fait bouger le bateau
            if (this.lestouches.left.isDown) //si on appuye sur la touche gauche
            {
                this.scene.matter.body.applyForce(this.body, this.body.position, { x: -this.vitesseLaterale, y: 0 });
            }
            else if (this.lestouches.right.isDown)  //si on appuye sur la touche droite
            {
                this.scene.matter.body.applyForce(this.body, this.body.position, { x: +this.vitesseLaterale, y: 0 });
            }
            else //si on appuye sur rien
            {
                const resistanceDrag = 1.1;
                this.setVelocityX(this.body.velocity.x / resistanceDrag); //ne bouge pas
            }
        }

         if(this.estLance && (-this.body.velocity.y) < this.vitesse && !this.desactiverControles){
            this.scene.matter.body.applyForce(this.body, this.body.position, { x: 0, y: -this.acceleration });
        }else{
            //éviter l'overspeed
            if((-this.body.velocity.y) > this.vitesse)    {
                this.setVelocityY(-this.vitesse);
            }                
        }
      

    }


    //change la vitesse de déplacement de gauche à droite
    changerVitesseLaterale(vitesseLaterale) {
        this.vitesseLaterale = vitesseLaterale;
    }

    changerVitesse(vitesse) {
        this.vitesse = vitesse;
    }


    lancerAnimation(){
        this.play('bateauEau');
        this.scene.sonBateau.play();
    }

    arreterAniation(){
        this.anims.stop();
        this.setTexture('bateau');
        this.scene.sonBateau.stop();
    }

    lancer(){
        this.estLance = true;
    }

    limiterPosition() {
        const distanceBordEcran = 100;

        if (this.body.position.x < distanceBordEcran) {
            this.setPosition(distanceBordEcran, this.body.position.y);
            this.setVelocityX(0);
        }
        if (this.body.position.x >= this.scene.cameras.main.width - distanceBordEcran) {
            this.setPosition(this.scene.cameras.main.width - distanceBordEcran, this.body.position.y);
            this.setVelocityX(0);
        }

    }

    bougerPirate(){

        let currentPoint = new Phaser.Geom.Point(this.x,this.y);
        let pointToMoveTo = new Phaser.Geom.Point(this.scene.poulpe.x, this.scene.poulpe.y);
        this.imagePirate.rotation = Phaser.Math.Angle.BetweenPoints(currentPoint, pointToMoveTo) + Phaser.Math.TAU;

        this.imagePirate.y = this.y-150;
        this.imagePirate.x = this.x;

       

    }

    faireAnimations(){
        if(this.anims.isPlaying && ((Math.abs(this.body.velocity.x) + Math.abs(this.body.velocity.y)) < 1)) {
            this.arreterAniation();
        }

        if(!this.anims.isPlaying && ((Math.abs(this.body.velocity.x) + Math.abs(this.body.velocity.y)) >= 1.5)) {
            this.lancerAnimation();
        }
    }
}
