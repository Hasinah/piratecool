export default class Pouple extends Phaser.Physics.Matter.Sprite { // Sprite -> peut être animé
    //nos variables contenu dans cet objet
    vitesse
    scene
    acceleration

    entrainRalentir
    tempsDebutRalentir
    estLance
    estAVitesse
    rattraperJoueur



    //Notre bateau joueur
    constructor(scene, x, y, vitesse, acceleration) {
        var poulpe = '-6.5 105.5 -19.5 54.5 -51.5 81.5 -82.5 73.5 -83.5 46.5 -82.5 18.5 -77.5 -18.5 -116.5 -49.5 -93.5 -71.5 -49.5 -82.5 -7.5 -111.5 34.5 -88.5 92.5 -74.5 99.5 -45.5 110.5 -13.5 114.5 25.5 95.5 47.5 65.5 51.5 25.5 33.5 40.5 70.5 12.5 105.5';

        super(scene.matter.world, x, y, 'poulpe', 0, { shape: { type: 'fromVerts', verts: poulpe, flagInternal: true }, frictionAir: 0, label: 'poulpe' });
        this.scene = scene;
        this.setMass(999);
        this.setOrigin(0.5, 0.5);
        this.setFixedRotation();
        this.setDepth(2);
        this.setCollisionCategory(2);
        scene.add.existing(this); //ajoute a la scene sinon ça apparait pas



        this.acceleration = acceleration;



        this.entrainRalentir = false;
        this.estLance = false;
        this.estAVitesse = false;
        this.rattraperJoueur = false;

        this.changerVitesse(vitesse);
    }


    update() {

        this.faireAnimations();

        this.faireBouger();
       
        this.evitementIntelligent();

    }



    changerVitesse(vitesse) {
        this.vitesse = vitesse
        this.vitesseLaterale = vitesse * 4;
    }

    ralentir() {
        console.log("ralentir");


        this.changerVitesse(this.vitesse / 2);
        this.setCollidesWith(0);
        //   console.log("entrain de ralentir");
        this.entrainRalentir = true;
        this.tempsDebutRalentir = Date.now();

        this.anims.stop();
        this.setTexture('poulpeFatigue');

        console.log(this);
    }

    sEnfuir() {
        this.changerVitesse(this.vitesse * 2);
    }

    faireAnimations(){
         if (!this.entrainRalentir && this.anims.isPlaying && ((Math.abs(this.body.velocity.x) + Math.abs(this.body.velocity.y)) < 1)) {
            this.arreterAnimation();
        }

        if (!this.entrainRalentir && !this.anims.isPlaying && ((Math.abs(this.body.velocity.x) + Math.abs(this.body.velocity.y)) >= 1.5)) {
            this.lancerAnimation();
        }

        if (!this.rattraperJoueur && this.entrainRalentir && (this.y >= this.scene.bateau.y - 150 || Date.now() >= this.tempsDebutRalentir + 5000)) {
            this.rattraperJoueur = true;
            this.scene.poulpeArriveAuNiveauDuJoueur();
        }        
    }


    lancerAnimation() {
        this.play('poulpeEau');
    }

    arreterAnimation() {
        this.anims.stop();
        this.setTexture('poulpe');
    }



    lancer() {
        this.estLance = true;
    }


    faireBouger() {

        if (this.estLance) {
            if (!this.estAVitesse) {
                if ((-this.body.velocity.y) < this.vitesse) {
                    this.scene.matter.body.applyForce(this.body, this.body.position, { x: 0, y: -this.acceleration });
                } else {
                    this.estAVitesse = true;
                }                
            } else {
                this.setVelocityY(-this.vitesse);

                const yPoulpeTropProcheBateau = 350;

                if (!this.entrainRalentir && this.scene.bateau.y - this.y < yPoulpeTropProcheBateau) {
                    this.setVelocityY(-this.vitesse * 2);
                    console.log("Boost du poulpe");
                }
            }

        }

        const resistanceDrag = 1.1;
        this.setVelocityX(this.body.velocity.x / resistanceDrag); 
    }

    evitementIntelligent() {
         let poulpeY = this.y;

        let deltaYPlusPetit = 9999999;
        let ileLaPlusProche = null;


        const deltaXAnticipationParMs = 15;
        let anticipation = (deltaXAnticipationParMs * this.vitesse);
        //   console.log("s",poulpeY);
        this.scene.iles.arrayDynamique.forEach(function(child) {
            //        console.log(child.y);
            let distance = Math.abs(child.y - (poulpeY - anticipation));
            if (distance < deltaYPlusPetit) {
                ileLaPlusProche = child;
                deltaYPlusPetit = distance;
            }
        });   

        if (ileLaPlusProche === null) { return; } 

        let deltaX = this.x - ileLaPlusProche.x; // si > 0, le poule est à droite de l'ile, sinon, il est à gauche (ou au centre pour 0)
        let deltaY = deltaYPlusPetit / 2;

        let deltaBoth = Math.sqrt(deltaX * deltaX + deltaY * deltaY);

        const cibleDistanceIle = 450;
        const exagerationX = 1.5;

        deltaX = deltaX / exagerationX;

        let deltaaccell = (cibleDistanceIle - deltaBoth) / cibleDistanceIle;
        if (deltaaccell < 0.01) { deltaaccell = 0 };
        deltaaccell = deltaaccell * deltaaccell


        if (deltaX > 0) //a droite, mais pas assez
        {
            //console.log("droite");
            this.scene.matter.body.applyForce(this.body, this.body.position, { x: this.vitesseLaterale * deltaaccell, y: 0 });
        }
        else {
            if (deltaX < 0) {  //a gauche mais pas assez
   
                //console.log("gauche");
                this.scene.matter.body.applyForce(this.body, this.body.position, { x: -this.vitesseLaterale * deltaaccell, y: 0 });
            }
        }
        if (deltaaccell === 0) //si on est au bon endroit
        {
            //console.log("aucun");

            const gentilesseCentrage = 4;
            const distanceMaxDuCentre = 300;


            if (this.x > this.scene.cameras.main.width / 2 + distanceMaxDuCentre) {
                this.scene.matter.body.applyForce(this.body, this.body.position, { x: this.vitesseLaterale * Phaser.Math.FloatBetween(-1, 0) / gentilesseCentrage, y: 0 });
            }
            else {
                if (this.x < this.scene.cameras.main.width / 2 - distanceMaxDuCentre) {
                    this.scene.matter.body.applyForce(this.body, this.body.position, { x: this.vitesseLaterale * Phaser.Math.FloatBetween(0, +1) / gentilesseCentrage, y: 0 });            
                }
            }
        }
    
    }
}
