import Ile from '../objects/ile.js'
import GroupeDynamique from '../objects/groupeDynamique.js'

export default class Iles extends GroupeDynamique {
  scene

  //Notre bateau joueur
  constructor(scene, arrayIles) {
    super(scene, arrayIles);
    this.scene = scene;
  }

  getLabel() {
    return 'ile';
  }


  newDynamique(positionX, positionY) {
    let ile = new Ile(this, this.scene, positionX, positionY, Phaser.Math.Between(0, 3));
    ile.lancerAnimation();
    return ile;
  }

  collisionObjet(bodyIle, bodyAutre) {
    if (bodyAutre.label === 'bateau') {
      this.scene.collisionIle();
    }
  }
}
