import Dynamique from '../objects/dynamique.js'

export default class Etoile extends Dynamique {
   vieRestante
   definirSensor
   grossissementRestant

   pasDeGrossisement = -999;
   vieRestanteNonApplicable = -1;

   constructor(etoiles, scene, x, y, apparaitPetit) {
      let scale = 1;
      if(apparaitPetit) {
         scale = 0.001;
      }
      super(etoiles, scene, x, y, 'etoile', 0, {frictionAir: 0.0, label: 'etoile'}, scale); //On appelle le constructeur d'image

      this.setBody({
        type: 'circle',
        radius: 35*scale,
      });
      this.body.label = 'etoile';
      this.setMass(0.00001);
      if(apparaitPetit) {
         this.setCollidesWith(0);
      }

      this.vieRestante = -1;
      this.definirSensor = false
      this.grossissementRestant = this.pasDeGrossisement;
   }

   update() {
      if(this.grossissementRestant !== this.pasDeGrossisement){

         const vitesseGrossissement = 3;
         this.grossissementRestant-=vitesseGrossissement;

         this.setScale((100-this.grossissementRestant)/100);

         if(this.grossissementRestant <= 0) {
            this.grossissementRestant = this.pasDeGrossisement;
            this.setCollidesWith(0xffff-0x2);
         }
      }


      if (this.vieRestante !== this.vieRestanteNonApplicable) {
         this.vieRestante--;
         this.setAlpha(this.vieRestante / 100);
      }

      if (this.vieRestante === 0) {
         this.groupeDynamique.retirerDynamique(this);
      }
   }

   lancerDestruction() {
      if (this.vieRestante === this.vieRestanteNonApplicable) {
         this.vieRestante = 100;
      }
   }

   estPasTouche() {
      return this.vieRestante === this.vieRestanteNonApplicable;
   }

   lancerGrossissement(){
      this.grossissementRestant = 100;
   }

}
