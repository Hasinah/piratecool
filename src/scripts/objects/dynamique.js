export default class Dynamique extends Phaser.Physics.Matter.Sprite {
   groupeDynamique

   constructor(groupeDynamique, scene, x, y, nom, sprite, parametres, scale) {
      if(scale === undefined){
         scale = 1;
      }
      super(scene.matter.world, x, y, nom, sprite, parametres);


      this.setScale(scale);
      scene.add.existing(this);
      this.groupeDynamique = groupeDynamique;
   }

   update() {
   }

}
