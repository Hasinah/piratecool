import Requin from '../objects/requin.js'
import GroupeDynamique from '../objects/groupeDynamique.js'

export default class Requins extends GroupeDynamique {

  //Notre bateau joueur
  constructor(scene, arrayRequins) {
    super(scene, arrayRequins);
  }

  getLabel() {
    return 'requin';
  }

  newDynamique(positionX, positionY) {
    return new Requin(this, this.scene, positionX, positionY);
  }

  collisionObjet(bodyRequin, bodyAutre) {
    if (bodyAutre.label === 'bateau') {
      let copyThis = this;
      copyThis.scene.collisionRequin();        
    } else {
    }
  }

}
