export default class MonLogo extends Phaser.GameObjects.Sprite {
  constructor(scene, x, y) {
    super(scene, x, y, 'mon-logo')
    scene.add.existing(this)
  }
}
