import 'phaser'
import '@babel/polyfill'

import MainScene from './scenes/mainScene'
import PreloadScene from './scenes/preloadScene'
import GameOver from './scenes/gameOver'
import LanceurNiveau from './scenes/lanceurNiveau'
import NiveauFini from './scenes/niveauFini'
import MenuStart from './scenes/menuStart'
import Enigmes from './scenes/enigmes'
import Ending from './scenes/ending'
import EndingRate from './scenes/endingRate'
const DEFAULT_WIDTH = 1280
const DEFAULT_HEIGHT = 720

const config = {
  type: Phaser.AUTO,
  backgroundColor: '#000000',
  scale: {
    parent: 'Pirates cool',
    mode: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH,
    width: DEFAULT_WIDTH,
    height: DEFAULT_HEIGHT
  },
  scene: [PreloadScene, MainScene, GameOver, LanceurNiveau, NiveauFini,MenuStart,Enigmes,Ending,EndingRate],
  physics: {
    default: 'matter',
    matter: {
      debug: false,
      //pas de gravité
    }
  }
}

window.addEventListener('load', () => {
  const game = new Phaser.Game(config)
})
