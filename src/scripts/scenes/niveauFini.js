export default class NiveauFini extends Phaser.Scene {
  niveau
  nbrPoints
  poulpeSEnfuit
  
  init(parametres) {
    this.niveau = parametres.niveau;
    this.nbrPoints = parametres.nbrPoints;
    this.poulpeSEnfuit = parametres.poulpeSEnfuit;
    console.log("NiveauFini", parametres);

  }

  constructor() {
    super({ key: 'NiveauFini' })
  }

  create() {   


    let texteEnfuit =  [`Il s'est enfui, mais on l'aura la prochaine fois...`,`tu y es arrivé, bravo !`,`${this.nbrPoints} pièces collectées`];
    let texteAttrape =  [`On l'a eut !!`,`Félicitations`,`${this.nbrPoints} pièces collectées`];
    
    let leBonTexte = texteEnfuit;
    if(!this.poulpeSEnfuit) {
      leBonTexte = texteAttrape;
    }

    this.add
      .text(this.cameras.main.width / 2, this.cameras.main.height / 2,leBonTexte, {
        color: '#FFFFFF',
        fontSize: 24,
        align: 'center',
      })
      .setOrigin(0.5, 0.5)

    this.cameras.main.setBackgroundColor(0x000000);


    let copyThis = this;
    setTimeout(function() { copyThis.scene.start('MenuStart', { niveau: copyThis.niveau + 1, nbrPoints: copyThis.nbrPoints }) }, 3000);

  }

  update() {

  }

}