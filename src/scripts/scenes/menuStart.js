export default class MenuStart extends Phaser.Scene {
  niveau
  nbrPoints
  textStart
  sonBouton
  sonHome

  init(parametres) {
    this.niveau = parametres.niveau;
    this.nbrPoints = parametres.nbrPoints;
    console.log("MenuStart", parametres);

  }

  constructor() {
    super({ key: 'MenuStart' })
  }

  create() {   

    let textePremierNiveaux = [`Niveau ${this.niveau + 1}`,`Le poulpe s'est enfuit avec notre trésor !`,`Suit-le, mais gare aux iles et aux requins.`,`On dirait qu'il perd des pièces ... `,`Utilises les flèches  du clavier `,`Droite/Gauche! à toi à jouer!!!`];
    let texteNiveauEnigmes = [`Le poulpe se rend !`,`Cependant, il te demande de répondre à`,`quelques énigmes pour avoir le trésor.`,``,`tu as collecté ${this.nbrPoints} pièces.`,`Peut-être pourra-tu le soudoyer ?`];


    let leBonTexte = "[manquant]";

    if (this.niveau < 4) {
      leBonTexte = textePremierNiveaux;
    } else {
      leBonTexte = texteNiveauEnigmes;
    }

    this.add.image(0, 0, 'menu').setOrigin(0).setDepth(-1);

    this.textStart = this.add
      .image(this.cameras.main.width / 2, this.cameras.main.height / 2 + 120, `start`)
      .setOrigin(0.5, 0.5).setInteractive();

    this.cameras.main.setBackgroundColor(0x000000);


    let text = this.add
      .text(this.cameras.main.width / 2, this.cameras.main.height / 2 - 45, leBonTexte, {
        color: '#5e2a0e',
        fontSize: 32,
        align: 'center'
      })
      .setOrigin(0.5, 0.5);

    let copyThis = this;
    this.textStart.on('pointerdown', () => { copyThis.textStart.setTexture('startPresse');  });
    this.textStart.on('pointerup', () => { copyThis.sonHome.stop(); copyThis.sonBouton.play(); setTimeout(function() {copyThis.scene.start('LanceurNiveau', { niveau: copyThis.niveau, nbrPoints: copyThis.nbrPoints });}, 700); });
    this.textStart.on('pointerover', () => { copyThis.textStart.setTexture('startHover'); });
    this.textStart.on('pointerout', () => { copyThis.textStart.setTexture('start'); });


    this.sonBouton = this.sound.add("sonBouton", { loop: false, volume: 1 });

    this.sonHome = this.sound.add("sonHome", { loop: false, volume: 0.1 });

    this.sonHome.play()

  }

  update() {

  }

}