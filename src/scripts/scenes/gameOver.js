export default class GameOver extends Phaser.Scene {
  niveau
  nbrPoints

  init(parametres) {
    this.niveau = parametres.niveau;
    this.nbrPoints = parametres.nbrPoints;
    console.log("GameOver", parametres);

  }

  constructor() {
    super({ key: 'GameOver' })
  }

  create() {   
    this.add
      .text(this.cameras.main.width / 2, this.cameras.main.height / 2, `Raté ! Re-tentes ta chance.`, {
        color: '#FFFFFF',
        align: 'center',
        fontSize: 24
      })
      .setOrigin(0.5, 0.5)

    this.cameras.main.setBackgroundColor(0x000000);


    let copyThis = this;
    setTimeout(function() { copyThis.scene.start('LanceurNiveau', { niveau: copyThis.niveau, nbrPoints: 0 }) }, 3000);

  }

  update() {

  }

}