export default class Ending extends Phaser.Scene {
nbrPoints
textStart
sonEnding

  init(parametres) {
    this.nbrPoints = parametres.nbrPoints;
    console.log("Ending", parametres);

  }

  constructor() {
    super({ key: 'Ending' })
  }

  create() {   


    this.add.image(0, 0, 'ending').setOrigin(0).setDepth(-1);

    this.textStart = this.add
      .text(this.cameras.main.width / 2 + 450, this.cameras.main.height / 2 , `Bravoooo!`, {
        color: '#5e2a0e',
        fontSize: 70,
        stroke: '#ddd417',
        strokeThickness: 3,
        fontStyle: 'bold'
        })
      .setOrigin(0.5, 0.5).setInteractive();

    let copyThis = this;
    this.textStart.on('pointerdown', () => { copyThis.textStart.setColor('#FF0000'); });
    this.textStart.on('pointerup', () => { copyThis.sonEnding.stop(); copyThis.scene.start('MenuStart', { niveau: 0, nbrPoints: 0 }); });
    this.textStart.on('pointerover', () => { copyThis.textStart.setColor('#ddd417'); });
    this.textStart.on('pointerout', () => { copyThis.textStart.setColor('#5e2a0e'); });

    this.sonEnding = this.sound.add("sonEnding", { loop: true, volume: 0.1 });
    this.sonEnding.play();
  }

  update() {

  }

}