export default class LanceurNiveau extends Phaser.Scene {
  niveau
  nbrPoints


  init(parametres) {
    this.niveau = parametres.niveau;
    this.nbrPoints = parametres.nbrPoints;
    console.log("LanceurNiveau", parametres);

  }

  constructor() {
    super({ key: 'LanceurNiveau' })
  }

  create() {   



    let reglages = [{ vitesse: 3, dureeNiveau: 50, poulpeSEnfuit: true },
                    { vitesse: 5, dureeNiveau: 50, poulpeSEnfuit: true },
                    { vitesse: 6, dureeNiveau: 50, poulpeSEnfuit: true },
                    { vitesse: 7, dureeNiveau: 30, poulpeSEnfuit: false }];

    if (this.niveau < reglages.length) {
      this.scene.start('MainScene', { vitesse: reglages[this.niveau].vitesse, nbrPoints: this.nbrPoints, niveau: this.niveau, dureeNiveau: reglages[this.niveau].dureeNiveau, poulpeSEnfuit: reglages[this.niveau].poulpeSEnfuit });

    } else {
      this.scene.start('Enigmes', { nbrPoints: this.nbrPoints});      
    }
  }

  update() {

  }

}