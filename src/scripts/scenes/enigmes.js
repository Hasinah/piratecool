export default class Enigmes extends Phaser.Scene {
  nbrPoints

  enigmesFaites

  enigmes
  question
  repA
  repB
  repC
  repBonne

  texteQuestion
  texteA
  texteB
  texteC
  texteSauter
  compteurPoints

  nbrEchec

  sonSucces
  sonEchec
  sonBouton
  sonPiece
  sonEnigme

  init(parametres) {
    this.nbrPoints = parametres.nbrPoints;
    console.log("Enigmes", parametres);

  }

  constructor() {
    super({ key: 'Enigmes' })
  }

  create() { 
    this.enigmes = [{question:`Quel animal utilise le sonar pour se localiser ?`, reponses : [`Dolphin`,`Roquin`,` vièber`]}, {question:`Quel est le poisson dangereux?`, reponses : [` Orge`,`Papillon`,` Chiruhien`]} , {question:`Combien de tenticules a notre poulpe?`, reponses : [`8`,`6`,`10`]} , {question:`Quel poisson lorsque sort de la mer il gonfle?`, reponses : [`Pufferfish`,` Hawkfish`,` Bigeye fish`]}];  

    this.enigmesFaites = [];
    this.nbrEchec = 0;

    this.add.image(0, 0, 'menu').setOrigin(0).setDepth(-1);
    
    this.texteQuestion = this.createBouton(this.cameras.main.width / 2, this.cameras.main.height / 2 - 75, undefined);
    this.texteA = this.createBouton(this.cameras.main.width / 2 - 300, this.cameras.main.height / 2).on('pointerdown', () => { this.texteA.setColor('#FF0000'); }).on('pointerover', () => { this.texteA.setColor('#ddd417'); }).on('pointerout', () => { this.texteA.setColor('#5e2a0e'); }).on('pointerup', () => { this.clickBouton(0); });
    this.texteB = this.createBouton(this.cameras.main.width / 2 + 0  , this.cameras.main.height / 2).on('pointerdown', () => { this.texteB.setColor('#FF0000'); }).on('pointerover', () => { this.texteB.setColor('#ddd417'); }).on('pointerout', () => { this.texteB.setColor('#5e2a0e'); }).on('pointerup', () => { this.clickBouton(1); });
    this.texteC = this.createBouton(this.cameras.main.width / 2 + 300, this.cameras.main.height / 2).on('pointerdown', () => { this.texteC.setColor('#FF0000'); }).on('pointerover', () => { this.texteC.setColor('#ddd417'); }).on('pointerout', () => { this.texteC.setColor('#5e2a0e'); }).on('pointerup', () => { this.clickBouton(2); });

    this.texteSauter= this.createBouton(this.cameras.main.width / 2, this.cameras.main.height / 2 + 120).on('pointerdown', () => { this.texteSauter.setColor('#FF0000'); }).on('pointerover', () => { this.texteSauter.setColor('#ddd417'); }).on('pointerout', () => { this.texteSauter.setColor('#5e2a0e'); }).on('pointerup', () => { this.clickSauter(); }).setAlign('center');

    this.compteurPoints = this.createBouton(this.cameras.main.width / 2 + 300, this.cameras.main.height / 2 - 130, undefined).setAlign('right');


    this.sonSucces = this.sound.add("sonSucces", { loop: false, volume: 0.2 });
    this.sonEchec = this.sound.add("sonEchec", { loop: false });
    this.sonBouton = this.sound.add("sonBouton", { loop: false, volume: 1 });
    this.sonPiece = this.sound.add("sonPiece", { loop: false, volume: 3 });    

    this.sonEnigme = this.sound.add("sonEnigme", { loop: true, volume: 1 });


    this.sonEnigme.play();
    this.enigmeSuivante();
  }


  createBouton(posX,posY){
    let variable = this.add
          .text(posX , posY, `toFill`, {
            color: '#5e2a0e',
            fontSize: 32
          })
          .setOrigin(0.5, 0.5).setInteractive();

    return variable;
  }

  preparerEnigme(){
    let numero;
    do{
      numero = Phaser.Math.Between(0,this.enigmes.length-1);
    } while(this.enigmesFaites.includes(numero)); 
    let ordre = this.genererOrdre();

    console.log('Enigme N',numero);

    this.question = this.enigmes[numero].question;
    this.repA = this.enigmes[numero].reponses[ordre[0]];
    this.repB = this.enigmes[numero].reponses[ordre[1]];
    this.repC = this.enigmes[numero].reponses[ordre[2]];
    this.repBonne = ordre.lastIndexOf(0);

    this.enigmesFaites.push(numero);


    this.texteQuestion.setText(this.question);
    this.texteA.setText(this.repA);
    this.texteB.setText(this.repB);
    this.texteC.setText(this.repC);
    if(this.nbrPoints >= 5) {
      this.texteSauter.setText(`Lui donner 5 pièces.`);
    } else {
      this.texteSauter.setText([`Pour 5 pièces il te`,`aurait donné la réponse.`]);      
    }
    this.compteurPoints.setText([`${this.nbrPoints} pièces`,`Toi ${this.enigmesFaites.length-this.nbrEchec-1} VS Lui ${this.nbrEchec}`]);


  }

  genererOrdre(){
    let ordre = [];

    do{
      let nombre = Phaser.Math.Between(0,2);
      if(!ordre.includes(nombre)){
        ordre.push(nombre);
      }
    } while(ordre.length < 3)

    return ordre;
  }

  clickBouton(choix){
    if(choix ==this.repBonne) {
      console.log("Bonne réponse !");
      this.sonSucces.play();
    } else {
      console.log("Mauvaise réponse ...");
      this.nbrEchec+= 1;
      this.sonEchec.play();
    }

    this.enigmeSuivante();

  }

  clickSauter(){
    const nombrePointsPourSauter = 5;

    if(this.nbrPoints <nombrePointsPourSauter) { return; }

    this.sonPiece.play();


    this.nbrPoints -= nombrePointsPourSauter;
    this.enigmeSuivante();
  }


  enigmeSuivante(){
    //this.sonBouton.play();

    const nombreReponsePourGagner = 3;
    const nombreReponsePourPerde = 3;

    if(this.nbrEchec >= nombreReponsePourPerde) {
      console.log("Game Over");

      let copyThis = this;
      setTimeout(function() {copyThis.stopSound(); copyThis.scene.start('EndingRate', {nbrPoints: this.nbrPoints });}, 1000);   
      return;
    }
    if(this.enigmesFaites.length-this.nbrEchec >= nombreReponsePourGagner) {
      console.log("Gagné");

      let copyThis = this;
      setTimeout(function() { copyThis.stopSoundSaufEffets(); copyThis.scene.start('Ending', {nbrPoints: this.nbrPoints });}, 1000);      
      return;
    }

    this.preparerEnigme();

  }

  update() {

  }

  stopSound() {
    this.sonSucces.stop();
    this.sonEchec.stop();
    this.sonBouton.stop();
    this.sonPiece.stop();  
    this.sonEnigme.stop();  
  }

  stopSoundSaufEffets() {
    this.sonBouton.stop();
    this.sonEnigme.stop();  
  }

}