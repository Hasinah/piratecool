export default class EndingRate extends Phaser.Scene {
nbrPoints
textStart
sonEndingRate

  init(parametres) {
    this.nbrPoints = parametres.nbrPoints;
    console.log("EndingRate", parametres);

  }

  constructor() {
    super({ key: 'EndingRate' })
  }

  create() {   


    this.add.image(0, 0, 'endingRate').setOrigin(0).setDepth(-1);

    this.textStart = this.add
      .text(this.cameras.main.width / 2 + 390, this.cameras.main.height / 2 + 100 , `Rejouer`, {
        color: '#5e2a0e',
        fontSize: 70,
        stroke: '#ddd417',
        strokeThickness: 3,
        fontStyle: 'bold'
        })
      .setOrigin(0.5, 0.5).setInteractive();

    let copyThis = this;
    this.textStart.on('pointerdown', () => { copyThis.textStart.setColor('#FF0000'); });
    this.textStart.on('pointerup', () => { copyThis.sonEndingRate.stop(); copyThis.scene.start('MenuStart', { niveau: 0, nbrPoints: 0 }); });
    this.textStart.on('pointerover', () => { copyThis.textStart.setColor('#ddd417'); });
    this.textStart.on('pointerout', () => { copyThis.textStart.setColor('#5e2a0e'); });


    this.sonEndingRate = this.sound.add("sonEndingRate", { loop: true, volume: 2 });
    this.sonEndingRate.play();
  }

  update() {

  }

}