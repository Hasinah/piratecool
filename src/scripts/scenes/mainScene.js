import MonLogo from '../objects/monLogo'
import CompteurPoints from '../objects/compteurPoints'
import Mer from '../objects/mer';
import Bateau from '../objects/bateau';
import Etoiles from '../objects/etoiles.js'
import Iles from '../objects/iles.js'
import Requins from '../objects/requins.js'
import Poulpe from '../objects/poulpe.js'

export default class MainScene extends Phaser.Scene {
  //parametres externes
  vitesse
  nbrPoints
  niveau
  dureeNiveau
  poulpeSEnfuit

  //parametres calculé dans la scene
  tempsNouvelleIle
  tempsDebut

  //nos objets
  compteurPoints
  mer
  bateau
  etoiles
  iles
  requins
  texteInfo
  poulpe

  finLancee
  triggerLancerGameOver
  triggerPasserNiveauSuivant


  sonSucces
  sonEchec
  sonBateau
  sonPiece
  sonHome

  init(parametres) {
    console.log("MainScene", parametres);

    this.vitesse = parametres.vitesse;
    this.nbrPoints = parametres.nbrPoints;
    this.niveau = parametres.niveau;
    this.dureeNiveau = parametres.dureeNiveau;
    this.poulpeSEnfuit = parametres.poulpeSEnfuit;

  }

  constructor() {
    super({ key: 'MainScene' })
  }

  create() {
    this.anims.create({
            key: 'bateauEau',
            frames: this.anims.generateFrameNumbers('bateauAnim', { start: 0, end: 9 }),
            frameRate: 5,
            repeat: -1
        });

    this.anims.create({
            key: 'ileAEau',
            frames: this.anims.generateFrameNumbers('ileAAnim', { start: 0, end: 5 }),
            frameRate: 3,
            repeat: -1,
            yoyo: true
        });
    this.anims.create({
            key: 'ileBEau',
            frames: this.anims.generateFrameNumbers('ileBAnim', { start: 0, end: 5 }),
            frameRate: 3,
            repeat: -1,
            yoyo: true
        });
    this.anims.create({
            key: 'ileCEau',
            frames: this.anims.generateFrameNumbers('ileCAnim', { start: 0, end: 5 }),
            frameRate: 3,
            repeat: -1,
            yoyo: true
        });

    this.anims.create({
            key: 'ileDEau',
            frames: this.anims.generateFrameNumbers('ileDAnim', { start: 0, end: 5 }),
            frameRate: 3,
            repeat: -1,
            yoyo: true
        });

    this.anims.create({
            key: 'poulpeEau',
            frames: this.anims.generateFrameNumbers('poulpeAnim', { start: 0, end: 5 }),
            frameRate: 5,
            repeat: -1,
        });

    this.anims.create({
            key: 'merEau',
            frames: this.anims.generateFrameNumbers('merAnim', { start: 0, end: 5 }),
            frameRate: 3,
            repeat: -1,
            yoyo: true
        });



    this.sonSucces = this.sound.add("sonSucces", { loop: false, volume: 0.2 });
    this.sonEchec = this.sound.add("sonEchec", { loop: false });
    this.sonBateau = this.sound.add("sonBateau", { loop: true, volume: 0.15 });
    this.sonPiece = this.sound.add("sonPiece", { loop: false, volume: 3 });
    this.sonHome = this.sound.add("sonHome", { loop: false, volume: 0.1 });



    this.matter.world.disableGravity();
    this.matter.world.pause();
    this.sonHome.play();


    this.tempsNouvelleIle = Date.now() + 2000;
    this.tempsDebut = Date.now();
    this.finLancee = false;

    this.triggerLancerGameOver = false;
    this.triggerPasserNiveauSuivant = false;


    //logo
    new MonLogo(this, this.cameras.main.width / 2, 0) //on crée le logo

    //compteur
    this.compteurPoints = new CompteurPoints(this, this.nbrPoints) // on crée le compteur de points

    //fond mer
    this.mer = new Mer(this);// on crée la mer

    //acteurs
    this.bateau = new Bateau(this, this.cameras.main.width / 2, this.cameras.main.height - 150, this.vitesse, 0.5) // on crée le bateau
    this.poulpe = new Poulpe(this, this.cameras.main.width / 2, this.cameras.main.height - 600, this.vitesse, 0.75) // on crée le bateau


    //objets
    this.etoiles = new Etoiles(this);
    this.iles = new Iles(this);
    this.requins = new Requins(this);


    //réglages vitesse
    this.changerVitesse(this.vitesse);
    this.bateau.changerVitesseLaterale(4);


    //reglages camera
    this.cameras.main.setDeadzone(this.cameras.main.width, 0);
    this.cameras.main.startFollow(this.bateau, true, 0.1, 0.1, 0, this.cameras.main.height / 2 - 150);


    const tempsAttenteDemarrage = 2;

    //lancer sous 2 secondes
    let copyThis = this;
    setTimeout(
      function() {
        copyThis.matter.world.resume();
        copyThis.poulpe.lancer();
        copyThis.bateau.lancer();
      }, tempsAttenteDemarrage*1000);
  }

  update() {
    this.compteurPoints.update();
    this.mer.update();


    //on fait rien si le monde bouge pas
    if(!this.matter.world.enabled){return;}


    if (Date.now() >= this.tempsDebut + this.dureeNiveau * 1000) {
      this.finPoulpe();    
    }

    if(this.triggerLancerGameOver) {
      this.lancerGameOver()
      return;
    }

    if(this.triggerPasserNiveauSuivant) {
      this.passerNiveauSuivant();
      return;
    }


    this.bateau.update();
    this.poulpe.update();
    this.etoiles.update();
    this.iles.update();
    this.requins.update();


    this.ajouterEtoile()
    this.ajouterIle()
  }


  //fonction qui permet de changer la vitesse de tout les objets en même temps pour faire accélerer ou ralentir le bateau
  changerVitesse(vitesse) {
    this.vitesse = vitesse;
  }



  collisionIle() {

    if (!this.finLancee) {
      this.finLancee = true;      

      this.stopSon();
      this.sonEchec.play();

      console.log('Fin ile');


      this.bateau.desactiverControles = true;
      this.bateau.body.frictionAir = 0.2;
      console.log("Collision Ile");
      let copyThis = this;
      setTimeout(
        function() {
          copyThis.triggerLancerGameOver = true;
        }, 1000);
    }
  }

  collisionRequin() {

    if (!this.finLancee) {
    this.finLancee = true;      


      this.stopSon();
      this.sonEchec.play();


      console.log('Fin requin');


      this.bateau.desactiverControles = true;
      this.bateau.body.frictionAir = 0.05;
      console.log("Collision Requin");
      let copyThis = this;
      setTimeout(
        function() {
          copyThis.triggerLancerGameOver = true;
        }, 1000);
    }
  }


  ajouterEtoile() {

    const chanceEtoile = 350;

    if (Phaser.Math.Between(0, chanceEtoile) == 50) {

      //etoile
      let positionX = Phaser.Math.Between(0, 1279);
      //this.etoiles.ajouterDynamique(positionX, this.cameras.main.worldView.y - 500, this.vitesse);

      //requin assoscié
      let directionX = 1;
      if (Phaser.Math.Between(0, 100) < 50) {
        directionX = -1;
      }
      let directionY = 1;
      if (Phaser.Math.Between(0, 100) < 50) {
        directionY = -1;
      }

      this.requins.ajouterDynamique(positionX + Phaser.Math.Between(100, +250) * directionX, this.cameras.main.worldView.y - 500 + Phaser.Math.Between(100, +250) * directionY, this.vitesse);

    }

    const chanceEtoileDuCoffre = 350;
    if (Phaser.Math.Between(0, chanceEtoileDuCoffre) == 50) {
      let etoile = this.etoiles.ajouterDynamique(this.poulpe.x+80, this.poulpe.y+30, this.vitesse, true);
      etoile.lancerGrossissement();
    }

  }

  ajouterIle() {
    if (Date.now() >= this.tempsNouvelleIle) {
      let positionX = Phaser.Math.Between(0, 1279);
      this.iles.ajouterDynamique(positionX, this.cameras.main.worldView.y - 1000, this.vitesse);

      const tempsSecondEntreIle = 20;

      this.tempsNouvelleIle = Date.now() + (tempsSecondEntreIle / (this.vitesse)) * 1000 * 1;
    }
  }

  finPoulpe() {
    if (!this.finLancee) {
      this.finLancee = true;  

      this.stopSon();
      this.sonSucces.play();

      console.log('Fin par poulpe');

      if (this.poulpeSEnfuit) {
        console.log('Poulpe s\'enfuit');

        this.poulpe.sEnfuir();
        let copyThis = this;
        setTimeout(
          function() {
            copyThis.triggerPasserNiveauSuivant = true;
          }, 2000);            

      } else {
        console.log('Poulpe fatigue');

        this.poulpe.ralentir();
        //poulpe s'occupe de lancer poulpeArriveAuNiveauDuJoueur()
      }     
    }
  }

  poulpeArriveAuNiveauDuJoueur() {
    this.poulpe.sEnfuir(); //mettre vitesse joueur
    // this.poulpe.animationParler();
    console.log("arriver niveau joueur");


    let copyThis = this;
    setTimeout(
      function() {
        //copyThis.scene.collisionIle();
        copyThis.triggerPasserNiveauSuivant = true;
      }, 500);
  }

  lancerGameOver() {

    this.stopSonFinScene();
    console.log("Changement scene vers gameover");
    this.scene.start('GameOver', { niveau: this.niveau, nbrPoints: this.compteurPoints.points })
  }

  passerNiveauSuivant() {

    this.stopSonFinScene();
    console.log("Changement scene vers niveau suivant");
    this.scene.start('NiveauFini', { niveau: this.niveau, nbrPoints: this.compteurPoints.points, poulpeSEnfuit: this.poulpeSEnfuit});      
  }

  lancerAnim(){
    this.bateau.play('bateauEau');
  }

  stopSon(){
    this.sonBateau.stop();
    this.sonSucces.stop();
    this.sonEchec.stop();    
    this.sonPiece.stop();
    this.sonHome.stop();
  }

  stopSonFinScene(){
    this.sonBateau.stop();    
    this.sonPiece.stop();
    this.sonHome.stop();
  }
}
