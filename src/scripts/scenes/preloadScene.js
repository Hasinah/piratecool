export default class PreloadScene extends Phaser.Scene {
  constructor() {
    super({ key: 'PreloadScene' })
  }

  preload() {

    this.load.image('mon-logo', 'assets/img/mon-logo.png') // image du logo
    this.load.spritesheet('merAnim', 'assets/img/mer.jpg', {frameWidth: 1281, frameHeight: 1440+50}) //image de la mer
    this.load.image('bateau', 'assets/img/bateau.png') //image du bateau
    this.load.spritesheet('bateauAnim', 'assets/img/bateauAnim.png', {frameWidth: 300, frameHeight: 500}) //image du bateau
    this.load.image('etoile', 'assets/img/etoile.png') //image d'une étoile
    this.load.image('ileA', 'assets/img/ileA.png') //image d'une étoile
    this.load.image('ileB', 'assets/img/ileB.png') //image d'une étoile
    this.load.image('ileC', 'assets/img/ileC.png') //image d'une étoile
    this.load.image('ileD', 'assets/img/ileD.png') //image d'une étoile
    this.load.spritesheet('ileAAnim', 'assets/img/ileAAnim.png', {frameWidth: 334, frameHeight: 336}) //image d'une étoile
    this.load.spritesheet('ileBAnim', 'assets/img/ileBAnim.png', {frameWidth: 444, frameHeight: 446}) //image d'une étoile
    this.load.spritesheet('ileCAnim', 'assets/img/ileCAnim.png', {frameWidth: 429, frameHeight: 437}) //image d'une étoile
    this.load.spritesheet('ileDAnim', 'assets/img/ileDAnim.png', {frameWidth: 448, frameHeight: 422}) //image d'une étoile

    this.load.image('poulpe', 'assets/img/poulpe.png') //image du bateau
    this.load.image('poulpeFatigue', 'assets/img/poulpeFatigue.png') //image du bateau
    this.load.spritesheet('poulpeAnim', 'assets/img/poulpeAnim.png', {frameWidth: 277, frameHeight: 259}) //image d'une étoile

    this.load.image('pirate', 'assets/img/pirate.png') //image du bateau
    this.load.image('requin', 'assets/img/requin.png') //image du bateau
    this.load.image('menu', 'assets/img/menu.png') //image du bateau
    this.load.image('ending', 'assets/img/ending.png') //image du bateau
    this.load.image('endingRate', 'assets/img/endingRate.png') //image du bateau
    this.load.image('start', 'assets/img/start.png') //image du bateau
    this.load.image('startHover', 'assets/img/startHover.png') //image du bateau
    this.load.image('startPresse', 'assets/img/startPresse.png') //image du bateau

    this.load.json('formes', 'assets/img/formeImg.json');


    this.load.audio("sonSucces", 'assets/mp3/sonSucces.mp3');
    this.load.audio("sonEchec", 'assets/mp3/sonEchec.mp3');
    this.load.audio("sonBateau", 'assets/mp3/sonBateau.mp3');
    this.load.audio("sonPiece", 'assets/mp3/sonPiece.mp3');
    this.load.audio("sonBouton", 'assets/mp3/sonBouton.mp3');
    this.load.audio("sonEnding", 'assets/mp3/sonEnding.mp3');
    this.load.audio("sonEndingRate", 'assets/mp3/sonEndingRate.mp3');
    this.load.audio("sonHome", 'assets/mp3/sonHome.mp3');
    this.load.audio("sonEnigme", 'assets/mp3/sonEnigme.mp3');
  }


  create() {
    this.scene.start('MenuStart', { niveau: 0, nbrPoints: 0 });

    /**
     * This is how you would dynamically import the mainScene class (with code splitting),
     * add the mainScene to the Scene Manager
     * and start the scene.
     * The name of the chunk would be 'mainScene.chunk.js
     * Find more about code splitting here: https://webpack.js.org/guides/code-splitting/
     */
    // let someCondition = true
    // if (someCondition)
    //   import(/* webpackChunkName: "mainScene" */ './mainScene').then(mainScene => {
    //     this.scene.add('MainScene', mainScene.default, true)
    //   })
    // else console.log('The mainScene class will not even be loaded by the browser')
  }
}
