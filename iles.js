import Ile from '../objects/ile.js'

export default class Iles extends Phaser.Physics.Arcade.Group  {

//Notre bateau joueur
 constructor(scene,arrayIles) {
    super(scene.physics.world, scene, arrayIles);

  }


  //change la vitesse de déplacement de gauche à droite
  changerVitesse(vitesse)
  {
    this.children.iterate(function (child) {
        child.changerVitesse(vitesse);
    });

  }

   ajouterIle(scene, positionX, positionY, vitesse) {
      let ile = new Ile(scene, positionX, positionY, Phaser.Math.Between(0,3));

      if(scene.verifierPasOverlap(ile))
      {
        this.add(ile, true);
        ile.changerVitesse(vitesse);      
      }
      else
      {
        ile.destroy();
      }
  }

  retirerViellesIles(scene, limite) {
    let iles = this;

    iles.getChildren().forEach( function (ile) {
      if(ile.y > limite)
      {
        iles.remove(ile,true,true);
      }
    });
  }

}
