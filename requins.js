import Requin from '../objects/requin.js'

export default class Requins extends Phaser.Physics.Matter.Group  {
vitesse

//Notre bateau joueur
 constructor(scene,arrayRequin) {
    super(scene.physics.world, scene, arrayRequin);

  }


  //change la vitesse de déplacement de gauche à droite
  changerVitesse(vitesse)
  {
    this.children.iterate(function (child) {
        child.changerVitesse(vitesse);
    });

  }

  ajouterRequin(scene, positionX, positionY, vitesse) {
      let requin = new Requin(scene, positionX, positionY);

      if(scene.verifierPasOverlap(requin))
      {
        this.add(requin, true);
        requin.changerVitesse(vitesse);      
      }
      else
      {
        requin.destroy();
      }
  }


  retirerViellesRequins(scene, limite) {
    let requins = this;

    requins.getChildren().forEach( function (requin) {
      if(requin.y > limite)
      {
        requins.remove(requin,true,true);
      }
    });
  }

}
