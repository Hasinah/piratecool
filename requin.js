export default class Requin extends Phaser.Physics.Arcade.Sprite {

 constructor(scene, x, y) {
    super(scene, x, y, 'requin'); //On appelle le constructeur d'image

    scene.add.existing(this);
    scene.physics.add.existing(this); //ajouter à la physique sinon ça apparait pas
  }

  changerVitesse(vitesse) {
   this.setVelocityY(vitesse);
  }

}
